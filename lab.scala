//Exercise 1

val someNumbers = sc.parallelize(1 to 1000)
val result = someNumbers.map(x => math.sqrt(x)).reduce(_ + _)
//val result = someNumbers.map(x => math.sqrt(x)).reduce(_ + _)


//Exercise 2

val people = sc.parallelize(Array(("Jane", "student", 1000),("Peter", "doctor", 100000), ("Mary", "doctor", 200000),("Michael", "student", 1000)))
val result = people.map(x => (x._2, x._3)).reduceByKey(_ + _).sortBy(_._1 ,true)
//Array[(String, Int)] = Array((doctor,300000), (student,2000))


//Exercise 3

val censusLines = sc.textFile("Input_data/census.txt")
val censusSplit = censusLines.map(_.split(", "))
val countryAge = censusSplit.map(r => (r(13), r(0).toInt)).filter(x => x._1 != "?")
countryAge.count
//31978 


//Exercise 4

countryAge.distinct.reduceByKey((x,y) => Math.max(x,y))


//Exercise 5

countryAge.distinct.reduceByKey((x,y) => Math.max(x,y)).sortBy(_._2 ,false).take(7)


//Exercise 6

val censusLines = sc.textFile("Input_data/census.txt")
val censusSplit = censusLines.map(_.split(", "))
val allPeople = censusSplit.map(r => (r(13), r(3), r(6), r(9)))
val filteredPeople = allPeople.filter(x => x._3 != "?")
filteredPeople.count
//30718
val canadians = filteredPeople.filter(x => x._1 == "Canada")
val americans = filteredPeople.filter(x => x._1 == "United-States")
canadians.count
//107
americans.count
//27504
val c = canadians.map(r => (r._3,(r._1,r._2,r._4)))
val a = americans.map(r => (r._3,(r._1,r._2,r._4)))
val repCandidates = c.join(a).map(_._2)
repCandidates.count
//325711
val includingDoctorate = repCandidates.filter(x => x._1._2 == "Doctorate" || x._2._2 == "Doctorate")
includingDoctorate.count
//31110
